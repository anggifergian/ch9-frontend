import React from 'react'
import { Table } from 'react-bootstrap'
import Moment from 'moment'
function HistoryComponents(props) {
  const DateFormatter = (date) => {
    return Moment(date).format('DD MMMM YYYY, h:mm a')
  }
  return (
    <div>
      <Table striped bordered hover className="binar-color-profile">
        <thead>
          <tr>
            <th>Date</th>
            <th>Game</th>
            <th>Score</th>
            <th>Result</th>
          </tr>
        </thead>
        <tbody>
          {props.gethsitory
            .map((x) => (
              <tr
                key={x.id}
                className={
                  x.score >= 3
                    ? 'background-win'
                    : x.score === 0
                    ? 'background-lose'
                    : ''
                }
              >
                <td>{DateFormatter(x.createdAt)}</td>
                <td>{x.game}</td>
                <td>{x.score}</td>
                <td>
                  {x.score >= 3 ? 'Menang' : x.score >= 1 ? 'Draw' : 'Kalah'}
                </td>
              </tr>
            ))
            .reverse()}
        </tbody>
      </Table>
    </div>
  )
}

export default HistoryComponents
