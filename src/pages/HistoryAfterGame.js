import React, { useEffect, useState } from 'react'
import { Button, Container, Table } from 'react-bootstrap'
import '../styles/historyAfterGame.css'
import Axios from 'axios'
import { getJwt } from '../services/authService'
function HistoryAfterGame() {
  const [uname, setUname] = useState([])
  const [histories, setHistories] = useState([])
  const getUname = () => {
    Axios.get('https://pacific-taiga-53059.herokuapp.com/whoami', {
      headers: {
        Authorization: getJwt(),
      },
    })
      .then((response) => setUname(response.data))
      .catch((error) => {
        console.log(error.response)
      })
  }
  const getHistories = () => {
    Axios.get('https://pacific-taiga-53059.herokuapp.com/api/player-logs', {
      headers: {
        Authorization: getJwt(),
      },
    })
      .then((response) => setHistories(response.data))
      .catch((error) => {
        console.log(error.response)
      })
  }
  useEffect(() => {
    getUname()
    getHistories()
  }, [])
  return (
    <Container>
      <div className="center-body">
        <h1 className="font-size-h1">Paper Rock Scissors</h1>
        <h5 className="my-3">{uname.username}</h5>
        <Button className="start-button-history my-5" href="/home">
          Back
        </Button>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Id Game</th>
              <th>Game</th>
              <th>Score</th>
              <th>Result</th>
            </tr>
          </thead>
          <tbody>
            {histories
              .map((data) => (
                <tr key={data.id}>
                  <td>{data.id}</td>
                  <td>{data.game}</td>
                  <td>{data.score}</td>
                  <td>
                    {data.score == 3
                      ? 'Menang'
                      : data.score == 1
                      ? 'Draw'
                      : 'Kalah'}
                  </td>
                </tr>
              ))
              .reverse()}
          </tbody>
        </Table>
      </div>
    </Container>
  )
}

export default HistoryAfterGame
