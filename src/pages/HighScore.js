import React, { useState, useEffect } from 'react'
import { Container, Table } from 'react-bootstrap'
import '../styles/leaderboard.css'

function HighScore() {
  const [high, setHigh] = useState([])

  const getHigh = async () => {
    
      const response = await fetch(
        'https://pacific-taiga-53059.herokuapp.com/api/leaderboard'
      )
      const parseResponse = await response.json()
      setHigh(parseResponse)
    
  }
  useEffect(() => {
    getHigh()
  }, [])
  let value = 0;
  return (
    <div>
      <Container>
        {}
        <div className="text-center my-5">
          <div className="font-size-30">LEADERBOARD</div>
        </div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Username</th>
              <th>Score</th>
            </tr>
          </thead>
          <tbody>
            {high.map((data) => (
              <tr key={data.id}>
                <td>{value = value + 1}</td>
                <td>{data.username}</td>
                <td>{data.score}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  )
}

export default HighScore
