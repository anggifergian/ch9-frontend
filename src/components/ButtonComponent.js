import React from "react"

const ButtonComponent = ({ label, type, onClick }) => {
  return (
    <button
      className={`btn ${type}`}
      onClick={onClick}
      style={{ margin: 8, padding: 10 }}
    >
      {label}
    </button>
  )
}

export default ButtonComponent
