import http from './httpService'
import { apiUrl } from '../config/config.json'
import jwtDecode from 'jwt-decode'

const endPoint = `${apiUrl}/login`
const tokenKey = 'binar.access.token'
const refreshKey = 'binar.refresh.token'

http.setJwt(getJwt())

// login
export async function login(username, password) {
  const { data } = await http.post(endPoint, {
    username: username,
    password: password,
  })

  // if (data) {
  //   for (var k in data) {
  //     localStorage.setItem(k, data[k])
  //   }
  // }

  localStorage.setItem(tokenKey, data.accessToken)
  localStorage.setItem(refreshKey, data.refreshToken)
  // >>>>>>> f1617576a5e21b662b31594ee37e9aa639140f83
}

export function logout() {
  localStorage.removeItem(tokenKey)
  localStorage.removeItem(refreshKey)
}

export function getCurrentUser() {
  try {
    const token = localStorage.getItem(tokenKey)
    return jwtDecode(token)
  } catch (err) {
    return null
  }
}

export function getJwt() {
  return localStorage.getItem(tokenKey)
}

// export function getNewAccessToken() {
//   const refresh = localStorage.getItem('refreshToken')
//   const request = await http.post(config.apiUrl + 'refresh-token', {
//     refreshToken: refresh,
//   })
//   if (request) {
//     for (var k in request) {
//       localStorage.setItem(k, request[k])
//     }
//   }
// }

const auth = {
  login,
  logout,
  getCurrentUser,
  getJwt,
}

export default auth
