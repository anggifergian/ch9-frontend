export const Profile = () => {
  return [
    {
      first_name: 'Binar',
      last_name: 'User Testing',
      user_name: 'HalloBinar',
      user_email: 'Binar@binar.com',
      photo: 'profile2.png',
    },
  ];
};
export const History = () => {
  return [
    {
      tgl: '12-12-12',
      score: '0',
      id_game: '121',
    },
    {
      tgl: '11-12-20',
      score: '3',
      id_game: '123',
    },
    {
      tgl: '01-10-12',
      score: '1',
      id_game: '123',
    },
    {
      tgl: '12-12-12',
      score: '3',
      id_game: '123124123123',
    },
    {
      tgl: '12-12-12',
      score: '1',
      id_game: '123124123123',
    },
  ];
};
export const Leaderboard = () => {
  return [
    {
      score: '110',
      username: 'bona',
    },
    {
      score: '109',
      username: 'viandika',
    },
    {
      score: '108',
      username: 'chrestella',
    },
    {
      score: '107',
      username: 'vikita',
    },
    {
      score: '106',
      username: 'william',
    },
    {
      score: '105',
      username: 'paul',
    },
    {
      score: '104',
      username: 'taufik',
    },
    {
      score: '0',
      username: 'tiko',
    },
    {
      score: '0',
      username: 'computer',
    },
    {
      score: '-100',
      username: 'anggi',
    },
  ];
};
